import React from 'react';
import './Game.css';
import $ from "jquery";
function Square(props) {
    return (
      <button id={'buttonStart'+props.id} className="square" onClick={props.onClick}>
        {props.value}
      </button>
    );
  }
  class Board extends React.Component {

    renderSquare(i) {
    
      return (

        <Square id={i}
          value={this.props.squares[i]}
          onClick={() => this.props.onClick(i)}
        />
      );
    }
  
    render() {
        var result =[];
        var index =0;
        for(let i =0;i<20;i++) {
            var rows =[];
            for(let j=0;j<20;j++) {
                rows.push(this.renderSquare(parseInt(index)));
                index++;
            }
            var row = <div className="board-row">{rows}</div>
            result.push(row);
        }
    
      return (
        <div>
            {result}
        </div>
      );
    }
  }
  export default class Game extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        history: [{
          squares: Array(400).fill(null)
        }],
        stepNumber: 0,
        xIsNext: true,
        selectedButton: '',
        arrResult: [],
        isIncrease: true
      };
    }
  
    handleClick(i) {
      const history = this.state.history.slice(0, this.state.stepNumber + 1);
      const current = history[history.length - 1];
      const squares = current.squares.slice();
      if (calculateWinner(squares) || squares[i]) {
        return;
      }
      squares[i] = this.state.xIsNext ? 'X' : 'O';
      this.setState({
        history: history.concat([{
          squares: squares
        }]),
        stepNumber: history.length,
        xIsNext: !this.state.xIsNext,
      });
    }
  
    jumpTo(step) {
      for(let i=0;i<400;i++) {
        $('#buttonStart'+i).removeClass('selectedButton');  
      }
      for(let i=0;i<this.state.history.length;i+=1) {
        $('#button'+i).removeClass('selectedButton');    
      }
      $('#button'+step).addClass('selectedButton');     
      this.setState({
        stepNumber: step,
        xIsNext: (step % 2) === 0,
      });
    }

    render() {

      const history = this.state.history;
      const current = history[this.state.stepNumber];
      const winner = calculateWinner(current.squares);

      var moves =[];

      if(this.state.isIncrease==true) {
        for(let i=0;i<history.length;i++) {
          const desc = i ?
          'Đi tới bước thứ ' + i :
          'Bắt đầu lại từ đầu';
          moves.push(          <li key={i}>
            <button id={'button'+i} onClick={() => this.jumpTo(i)}>{desc}</button>
            </li> );
        }
      } else {
        for(let i=history.length-1;i>=0;i--) {
          const desc = i ?
          'Đi tới bước thứ ' + i :
          'Bắt đầu lại từ đầu';
          moves.push(          <li key={i}>
            <button id={'button'+i} onClick={() => this.jumpTo(i)}>{desc}</button>
            </li> );
        }
      }

      let status;
      if (winner) {
        status = 'Xin chúc mừng, người chiến thắng là: ' + winner;
      } else {
        status = 'Lượt chơi tiếp theo: ' + (this.state.xIsNext ? 'X' : 'O');
      }
  
      return (
        
        <div className="game">
          <div className="game-board">
            <Board
              squares={current.squares}
              onClick={(i) => this.handleClick(i)}
            />
          </div>
          <div className="game-info">
            <div>{status}</div> 
            <div><button onClick={()=>{
                  
                    this.setState({
                      isIncrease: !this.state.isIncrease
                    });
                  
            }}>Sắp xếp các bước đi</button></div>
            <div style={{overflow: 'scroll', height: '80vh'} }>
            <ul>{moves}</ul>
            </div>
          </div>
        </div>
      );
    }
  }
  
  // ========================================
  function sapXep() {
    if(this.state.isIncrease==true) {
      this.setState({
        isIncrease: false
      });
    }
    
    if(this.state.isIncrease==false) {
      this.setState({
        isIncrease: true
      });
    }
  }

  
  function calculateWinner(squares) {
    var isSimilar = false;
    var countCheo=1;

    for (var i = 0; i < 20; i++) {
    
        var countNgang = 1;
        var countDoc = 1;
        var checkTraiNgang = true;
        var checkPhaiNgang = true;
        var checkTraiDoc = true;
        var checkPhaiDoc = true;
        for(var j=0;j<19;j++) {
            //Hàng ngang
            if(squares[i*20+j]!=null && squares[i*20+j+1]!=null) {
              
                if(squares[i*20+j] != squares[i*20+j+1]) {
                    countNgang = 1;
                } else {
                    countNgang++;
                    if(countNgang==5) {
                      for(var k=0;k<j;k++) {
                        if(squares[i*20+k]!=null) {
                          if(squares[i*20+k]!=squares[i*20+j+1]){
                            checkTraiNgang = false;
                          }
                        }

                      }
                      for(var k=18;k>=j+2;k--) {
                        if(squares[i*20+k]!=null) {
                          if(squares[i*20+k]!=squares[i*20+j+1]){
                            checkPhaiNgang = false;
                          }
                        }
                      }
                      if(checkTraiNgang==false&&checkPhaiNgang==false) {
                        countNgang = 1;
                      } else {
                        for(let count=j+1;count>j-4;count--) {
                          $('#buttonStart'+(i*20+count)).addClass('selectedButton');
                        }
                        return squares[i*20+j];
                      }
                    }
                }
            } else {
                countNgang = 1;
            }

            
            //Hàng dọc
            if (squares[j*20 + i] != null && squares[(j+1)* 20 + i] != null) {
                if(squares[j*20 + i] != squares[(j + 1)*20 + i]) {
                  countDoc=1;
                }  else {
                    countDoc++;
                    console.log(countDoc);
                    if (countDoc == 5) {
                      
                      for(var k=0;k<j;k++) {
                        if(squares[k*20+i]!=null) {
                          if(squares[k*20+i]!=squares[(j+1)*20+i]) {
                            checkTraiDoc = false;
                          }
                        }
                      }
                      for(var k=18;k>=j+2;k--) {
                        if(squares[k*20+i]!=null) {
                          if(squares[k*20+i]!=squares[(j+1)*20+i]) {
                            checkPhaiDoc = false;
                          }
                        }
                      }
                      if(checkPhaiDoc==false&&checkTraiDoc==false) {
                        countDoc =1;
                      } else {
                        for(let count=j+1;count>j-4;count--) {
                          $('#buttonStart'+(count*20+i)).addClass('selectedButton');
                        }
                        return squares[j * 20 + i];
                      }
                    }
                }
            } else {
                countDoc=1;
            }


        }
    }
    //Đường chéo
    for (var i = 0; i < 16; i++) {
      for(var j=0;j<16;j++) {
        if (squares[i*20 + j] != null && squares[(i+1)* 20 + j+1] != null) {
          if(squares[i*20 + j] != squares[(i+1)* 20 + j+1]) {
             countCheo =1;
          }  else {

            countCheo++;
              if (countCheo == 5) {
                for(let k = i+1;k>=i-3;k--) {
                  for(let count=j+1;count>=j-3;count--) {
                    console.log(k+" "+count);
                    $('#buttonStart'+(k*20+count)).addClass('selectedButton');
                    k--;
                  }
                }
               countCheo = 1;  
                 return squares[i * 20 + j];
             }
             i++;
         }
     } else {
      countCheo =1;
     }
      }
    }

    for (var i = 19; i >=5; i--) {
      for(var j=19;j>=5;j--) {
        if (squares[i*20 + j] != null && squares[(i+1)* 20 + j+1] != null) {
          if(squares[i*20 + j] != squares[(i+1)* 20 + j+1]) {
             countCheo =1;
          }  else {

            countCheo++;
              if (countCheo == 5) {
                for(let k = i-1;k<=i+3;k++) {
                  for(let count=j-1;count<=j+3;count++) {
                    console.log(k+" "+count);
                    $('#buttonStart'+(k*20+count)).addClass('selectedButton');
                    k++;
                  }
                }
               countCheo = 1;  
                 return squares[i * 20 + j];
             }
             i--;
         }
     } else {
      countCheo =1;
     }
      }
    }

    var duongCheoPhu = 1;
    for (var i = 0; i <16; i++) {
      for(var j=19;j>=5;j--) {
        if (squares[i*20 + j] != null && squares[(i+1)* 20 + j-1] != null) {
          if(squares[i*20 + j] != squares[(i+1)* 20 + j-1]) {
            duongCheoPhu =1;
          }  else {
            duongCheoPhu++;
              if (duongCheoPhu == 5) {
                for(let k = i+1;k>=i-3;k--) {
                  for(let count=j-1;count<=j+3;count++) {
                    console.log(k+" "+count);
                    $('#buttonStart'+(k*20+count)).addClass('selectedButton');
                    k--;
                  }
                }
                duongCheoPhu = 1;
                 return squares[i * 20 + j];
             }
             i++;
         }
     } else {
      duongCheoPhu =1;
     }
      }
    }


    for (var i = 0; i <16; i++) {
      for(var j=19;j>0;j--) {
        if (squares[i*20 + j] != null && squares[(i+1)* 20 + j-1] != null) {
          if(squares[i*20 + j] != squares[(i+1)* 20 + j-1]) {
            duongCheoPhu =1;
          }  else {
            duongCheoPhu++;
              if (duongCheoPhu == 5) {
                for(let k = i-1;k<=i+3;k++) {
                  for(let count=j+1;count>=j-3;count--) {
                    $('#buttonStart'+(k*20+count)).addClass('selectedButton');
                    k++;
                  }
                }
                duongCheoPhu = 1;
                 return squares[i * 20 + j];
             }
             i++;
         }
     } else {
      duongCheoPhu =1;
     }
      }
    }

    return null;
  }
    